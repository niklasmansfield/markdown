# En stor rubrik
## En lite mindre rubrik
### Ännu mindre

###### Sex brädgårdar är den minsta rubriken


Här under kommer tre dashes (man måste ha en blankrad annars räknas det som att man gör rubriker)

---

Här kommer tre underscores
___

och sist tre asterisker
***



med bara två asterisker **kan man få saker fetstilta**.

om man kör med bara en asterisk *så blir resultatet kursivt*.

underscores runt _texten gör den kursiv_.

En punktlista kan man göra med (lägg märke till blankraden)

* asterisker
* dashes, eller
* plustecken

***

Vi kan göra numrerade listor genom att

1. Skriva en etta
1. i början av varje rad.
1. Då tar markdown själv
1. hand om att numren blir rätt och i ordning.

***

En kodsnutt på en rad stoppar vi in mellan `fnuttar`, alltså tecknet `

Vill vi skriva flera rader så indenterar vi bara minst 4 steg

    def double(number)
        return 2 * number

alternativt påbörjar man och avslutar med s k fences, som är tre av fnuttarna i rad.

```
def doubledouble(number)
    number += number
    number += number
    return number
```

För en länk så skriver man länktexten inom hakparenteser och själva länken följer direkt i en vanlig parentes tillsammans med eventuellt tooltip.

[här hittar du patwic](http://www.patwic.com)

För att få in en bild gör du likadant men inleder raden med ett utropstecken:

![En elefant](http://dotnametechblog.files.wordpress.com/2010/05/dim.jpg "Värsta sjuka elefanten!")
